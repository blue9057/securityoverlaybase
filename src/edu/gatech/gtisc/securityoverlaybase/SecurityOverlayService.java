package edu.gatech.gtisc.securityoverlaybase;

import android.accessibilityservice.AccessibilityService;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

public class SecurityOverlayService extends AccessibilityService {
	
	public static String storedPackageNameString = "";

	@Override
	public void onAccessibilityEvent(AccessibilityEvent a11yEvent) {
		// Blocking null objects
		if(a11yEvent == null) return;
		
		// Get event type for A11y event
		int eventType = a11yEvent.getEventType();
		
		/*
		 * At here, we want to tries to catch app change for the example project.
		 * We watch for the TYPE_WINDOW_STATE_CHANGED for detecting app change.
		 * Refer to 
		 * http://developer.android.com/reference/android/view/accessibility/AccessibilityEvent.html#TYPE_WINDOWS_CHANGED
		 */
		
		if(eventType == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
			CharSequence pkgCharSeq = a11yEvent.getPackageName();
			if(pkgCharSeq != null) {
				String pkgString = pkgCharSeq.toString();
				if(!pkgString.equals(storedPackageNameString) && !pkgString.isEmpty()) {
					// App changed (different with previous app string
					storedPackageNameString = pkgString;
					Log.v("LOG_PACKAGE_NAME", "Currently running the app " + pkgString);
				}
			}
		}
		else {
			// We don't interested to the other events right now.
		}
	}

	@Override
	public void onInterrupt() {
		// TODO Auto-generated method stub
		
	}

}
